import React from 'react';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import BottomNavigator from './Components/BottomNavigator';
import { default as theme } from './theme.json';
import { mapping } from '@eva-design/eva';
import { default as customMapping } from './custom-mapping.json';
import RouterComponent from './Router';
import NavigationService from '../Services/NavigationService';
import { createAppContainer } from 'react-navigation';


const AppContainer = createAppContainer(RouterComponent);

const App = () => {
  console.disableYellowBox = true;
  
  return (
    <ApplicationProvider {...eva}
      theme={{ ...theme }}
      mapping={mapping}
      
      customMapping={customMapping}>
      <IconRegistry icons={EvaIconsPack} />
      <SafeAreaProvider>
        <AppContainer ref={navigatorRef => {
        NavigationService.setTopLevelNavigator(navigatorRef);
      }} />
        <BottomNavigator/>
      </SafeAreaProvider>
    </ApplicationProvider>
  );
};

export default App;
