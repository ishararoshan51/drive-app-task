import * as React from 'react';
import { StyleSheet, Dimensions, StatusBar, TouchableOpacity, ImageBackground, Keyboard, TouchableWithoutFeedback, Alert } from 'react-native';
import Svg, { Defs, LinearGradient, Stop, Path } from 'react-native-svg';
import ValidationComponent from 'react-native-form-validator';
import { Image, View } from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BlurView, VibrancyView } from "@react-native-community/blur";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { Icon, Button, MenuItem, OverflowMenu, Text, TopNavigationAction, Modal, Card, Input } from '@ui-kitten/components';
import RatingComponent from './Components/RatingComponent';
import * as theme from './theme.json';

const WIDTH = Dimensions.get('screen').width;
const { height, width } = Dimensions.get('window');

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[{ backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const location = React.createRef();
const dateAndtime = React.createRef();
const huboReading = React.createRef();

class Dashboard extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      menuVisible: false,
      modalVisible: false,
      datepickershow: false,
      timepickershow: false,
      date: null,
      time: null,
      location: "",
      hubo_reading: ""



    }

  }


  dropdownIcon = (props) => (
    <Icon {...props} name='arrow-ios-downward-outline' />
  );


  renderMenuAction = () => (
    <TouchableOpacity onPress={() => { this.setState({menuVisible:true})}} style={{ flexDirection: 'row' }} >
    <Text>Select Vehicle</Text>
    <Icon fill="#8a8a8a" style={{ width: 17, height: 20 }} name='arrow-ios-downward-outline'></Icon>
  </TouchableOpacity>

  );

  locationIcon = (props) => (
    <Icon {...props} name='radio-button-on' />

  );

  renderOverflowMenuAction = () => (
    <React.Fragment>
      <OverflowMenu
        anchor={this.renderMenuAction}
        visible={this.state.menuVisible}
        onBackdropPress={this.setState({ menuVisible: false })}>
        <MenuItem accessoryLeft={InfoIcon} title='About' />
        <MenuItem accessoryLeft={PrinterIcon} onPress={event => { this.setState({ menuVisible: false }, () => { this.props.navigation.navigate('PrinterSetup') }) }} title='Connect Printer' />
        <MenuItem accessoryLeft={BackupIcon} disabled={!this.state.userbility} onPress={event => { this.setState({ menuVisible: false }, () => { this.props.navigation.navigate('Backups') }) }} title='Backups' />
        <MenuItem accessoryLeft={LogoutIcon} onPress={event => { this.logOut() }} title='Logout' />
      </OverflowMenu>
    </React.Fragment>
  );
  componentDidMount() {
    this.setState({ date: new Date(), time: new Date() })//Set current date and time
  }

  onChangeDate = (event, selectedDate) => {//Pick date
    this.setState({ date: selectedDate, datepickershow: false, timepickershow: true })
  }

  onChangeTime = (event, selectedTime) => {//Pick time
    this.setState({ time: selectedTime, timepickershow: false })
  }

  convertDateTime() {//Converting date and time to String with in local zone
    var stillUtc_date = moment.utc(this.state.date).toDate();
    var local_date = moment(stillUtc_date).local().format('DD MMM YYYY');

    var stillUtc_time = moment.utc(this.state.time).toDate();
    var local_time = moment(stillUtc_time).local().format('h.mm a');
    return local_time.toString() + ", " + local_date.toString();
  }

  submit() {
    let validation = {
      dateAndtime: { 'rule': { required: true }, 'value': this.state.date },
      location: { 'rule': { required: true }, 'value': this.state.location },
      huboReading: { 'rule': { required: true }, 'value': this.state.hubo_reading },
    }
    this.validate(validation);
    if (this.isFormValid()) {
      this.setState({ modalVisible: false })
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView scrollEnabled={true}
          enableAutomaticScroll={true}
          behavior='padding'
          style={{ backgroundColor: 'white', width: "100%", height: "100%" }}>

          <MyStatusBar backgroundColor={'transparent'} barStyle="dark-content" />
          {/**Top Lable start */}
          <View style={{ height: 170, width: '100%' }}>
            <View style={{ height: width }} animation={'fadeInDownBig'} duration={800} delay={200}>
              <Svg width={width} height={width} viewBox="100 150 400 700">
                <Defs>
                  <LinearGradient x1="90.743%" y1="87.641%" x2="10.14%" y2="3.465%" id="prefix__a">
                    <Stop stopColor={"#E76A4E"} offset="10%" />
                    <Stop stopColor={"#E76A4E"} offset="100%" />
                  </LinearGradient>
                </Defs>
                <Path
                  d="m-4.39037,416.97872c225.90459,-167.35038 564.09312,88.4841 806.03827,-63.47773l-4.01269,-285.49957l-803.03067,7.57144l1.00509,341.40586z"
                  fill="url(#prefix__a)"
                  fillRule="evenodd"
                />
              </Svg>
            </View>

            <View style={styles.logo}>
              <View animation={'bounceIn'} duration={800} delay={1200} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image animation={'fadeInLeftBig'} duration={800} delay={600} style={styles.logo_icon}
                  source={require('../assets/icons/book.png')}
                />
                <Text status='primary' style={styles.logoTextBig}>DriveTime</Text>
              </View>
            </View>
          </View>
          {/**Top Lable End */}

          <View style={{ height: height - 200, width: '100%' }}>
            {/**Middle details view start */}
            <View animation={'bounceIn'} duration={800} delay={1200} style={{ flexDirection: 'row', width: '99%', alignItems: 'center', alignSelf: 'center' }}>
              <View style={[styles.topTile]}>
                
              <OverflowMenu
        anchor={this.renderMenuAction}
        visible={this.state.menuVisible}
        onBackdropPress={()=>{this.setState({ menuVisible: false })}}>
        <MenuItem  title='Vehicle 1' />
        <MenuItem  onPress={event => { this.setState({ menuVisible: false }) }} title='Vehicle 2' />
        <MenuItem  onPress={event => { this.setState({ menuVisible: false }) }} title='Vehicle 3' />
        <MenuItem  onPress={event => { this.setState({ menuVisible: false }) }} title='Vehicle 4' />
      </OverflowMenu>

                <Text category='h6' style={{ fontWeight: 'bold', marginTop: 10 }}>114,000Km</Text>
                <Text style={{ fontWeight: '400' }}>Hubometer</Text>

              </View>
              <View style={[styles.topTile, { borderStyle: 'solid', borderColor: theme['color-basic-400'], borderLeftWidth: 1, borderRightWidth: 1 }]}>
                <Text category='h1' style={{ fontWeight: 'bold' }}>Idle</Text>
                <Text style={{ fontWeight: '400' }}>Current Status</Text>
                <RatingComponent rate={1} />
              </View>
              <View style={[styles.topTile]}>
                <Text category='h3' style={{ fontWeight: 'bold' }}>3h</Text>
                <Text style={{ fontWeight: '400' }}>Next Break Due</Text>
              </View>
            </View>
            {/**Middle details view end */}
            {/*----------------------------------------------------------------------------------------*/}
            {/**Button section start */}
            <View style={{ height: height - 350, width: '100%' }}>

              <View style={styles.buttonRow}>
                <View animation={'fadeInDown'} duration={800} delay={800} style={styles.rowInside}>
                  <TouchableOpacity onPress={() => { this.setState({ modalVisible: true }) }} style={[styles.card, { backgroundColor: "#e4fde7" }]}>



                    {this.state.location != "" ?
                      <View style={styles.insideButton}>
                        <Text>Date and Time: {this.convertDateTime()}</Text>
                        <Text>Location: {this.state.location}</Text>
                        <Text>Hubo Reading: {this.state.hubo_reading}</Text>
                      </View> :
                      <View style={styles.insideButton}>
                        <Image style={styles.ButtonIcon}
                          source={require('../assets/icons/start-button.png')}
                        />
                        <Text category="h3" style={styles.ctaButtonText}  >START DRIVING</Text>
                      </View>

                    }


                  </TouchableOpacity>
                </View>
              </View>



              <View style={styles.buttonRow}>

                <View animation={'fadeInLeftBig'} duration={800} delay={600} style={[styles.rowInside, { width: '48%' }]}>
                  <TouchableOpacity onPress={() => { }} style={[styles.card, { backgroundColor: "#fdf2ba" }]}>
                    <View style={styles.insideButton}>
                      <Image style={styles.ButtonIcon}
                        source={require('../assets/icons/working-time.png')}
                      />
                      <Text category="c2" style={styles.ctaButtonText}  >START OTHER WORK</Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View animation={'fadeInRightBig'} duration={800} delay={600} style={[styles.rowInside, { width: '48%' }]}>
                  <TouchableOpacity onPress={() => { }} style={[styles.card, { backgroundColor: "#e1eefe" }]}>
                    <View style={styles.insideButton}>
                      <Image style={styles.ButtonIcon}
                        source={require('../assets/icons/timebreak.png')}
                      />
                      <Text category="c2" style={styles.ctaButtonText}  >START REST</Text>
                    </View>
                  </TouchableOpacity>
                </View>

              </View>

              <View style={styles.buttonRow}>

                <View animation={'fadeInUp'} duration={800} delay={600} style={styles.rowInside}>
                  <TouchableOpacity onPress={() => { }} style={[styles.card, { backgroundColor: "#fce8e9" }]}>
                    <View style={styles.insideButton}>
                      <Image style={styles.ButtonIcon}
                        source={require('../assets/icons/clock.png')}
                      />
                      <Text category="h3" style={styles.ctaButtonText}  >END OF DAY</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

            </View>
            {/**Button section end */}

            <View animation={'fadeInUp'} duration={800} delay={800} style={{ padding: 15 }}>
              <Text>Driver's Last Remarks:</Text>
              <Text category="h6" style={{ fontWeight: 'bold' }}>Gate access code is 8237452. ask for Mike</Text>
            </View>

          </View>

        </KeyboardAwareScrollView>

        <Modal
          visible={this.state.modalVisible}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => this.setState({ modalVisible: false })}>
          <Card style={styles.modalCard}>
            <View style={{ width: width - 50, alignItems: 'center', alignContent: 'center' }}>
              <Text category="h4" style={{ fontWeight: 'bold' }}>Start Driving</Text>
              <View style={{ width: width - 90, alignItems: 'center', alignContent: 'center', paddingTop: 20 }}>

                <TouchableWithoutFeedback style={{ width: '100%' }} onPress={() => { this.setState({ datepickershow: true }) }}>
                  <View style={{ width: '100%' }} >
                    <View style={{ width: '100%' }} pointerEvents="none">
                      <Input
                        ref={dateAndtime}
                        style={styles.textInput}
                        value={this.convertDateTime()}
                        label={evaProps => <Text {...evaProps} category='label'>Date and Time</Text>}
                        size={width < 368 ? 'medium' : 'large'}
                        placeholder='Date and Time'
                        caption={this.isFieldInError('dateAndtime') && this.getErrorsInField('dateAndtime').map(errorMessage => <Text category='c1' status='danger'>{errorMessage}</Text>)}
                        onChangeText={Value => this.setState({ location: Value })}
                      />
                    </View>
                  </View>
                </TouchableWithoutFeedback>


                <Input
                  ref={location}
                  style={[styles.textInput, { marginTop: 10 }]}
                  value={this.state.location}
                  label={evaProps => <Text {...evaProps} category='label'>Location</Text>}
                  accessoryRight={this.locationIcon}
                  size={width < 368 ? 'medium' : 'large'}
                  placeholder='Location'
                  caption={this.isFieldInError('location') && this.getErrorsInField('location').map(errorMessage => <Text category='c1' status='danger'>{errorMessage}</Text>)}
                  onChangeText={Value => this.setState({ location: Value })}
                  onSubmitEditing={(event) => {
                    huboReading.current.focus();
                  }}
                />

                <Input
                  ref={huboReading}
                  style={[styles.textInput, { marginTop: 10 }]}
                  value={this.state.hubo_reading}
                  label={evaProps => <Text {...evaProps} category='label'>Current Hubo Reading</Text>}
                  // accessoryRight={this.renderUsernameIcon}
                  size={width < 368 ? 'medium' : 'large'}
                  placeholder='Current Hubo Reading'
                  caption={this.isFieldInError('huboReading') && this.getErrorsInField('huboReading').map(errorMessage => <Text category='c1' status='danger'>{errorMessage}</Text>)}
                  onChangeText={Value => this.setState({ hubo_reading: Value })}
                  onSubmitEditing={(event) => {
                    this.submit()
                  }}
                />

                <Button style={{ width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}
                  onPress={() => this.submit()}>
                  CONFIRM AND LOG
          </Button>
                <Button appearance='ghost' style={{ marginTop: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}
                  onPress={() => this.setState({ modalVisible: false })}>
                  CANCEL
          </Button>


              </View>

            </View>



          </Card>


        </Modal>
        {this.state.datepickershow && (
          <DateTimePicker
            testID="dateTimePicker"
            value={this.state.date}
            mode={'date'}
            is24Hour={true}
            // display="default"
            onChange={this.onChangeDate}
            onCancel={() => { this.setState({ datepickershow: false }) }}
          />
        )}

        {this.state.timepickershow && (
          <DateTimePicker
            testID="dateTimePicker"
            value={this.state.time}
            mode={'time'}
            is24Hour={true}
            // display="default"
            onChange={this.onChangeTime}
            onCancel={() => { this.setState({ timepickershow: false }) }}
          />
        )}

      </View>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    alignContent: 'center',
    alignItems: 'center',
  },

  logo: {
    position: 'absolute',
    top: (width < 368 ? 30 : 50),
    //width: (width / 100) * 85,
    alignSelf: 'center'

  },
  logo_icon: {
    width: (width / 100) * 8,
    height: (width / 100) * 8,
    resizeMode: "contain",
  },
  logoTextBig: {
    fontSize: (width < 368 ? 30 : 40),
    fontWeight: 'bold',
    color: '#FFFFFF',
    letterSpacing: 0,
  },

  topTile: {
    width: '33%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',

  },

  buttonRow: {
    flexDirection: 'row',
    width: '100%',
    height: "33%",
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  rowInside: {
    width: '96%', height: '100%', padding: 7
  },
  ButtonIcon: {
    width: height / 100 * 8,
    height: height / 100 * 8
  },
  insideButton: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: '100%'
  },

  ctaButtonText: {
    fontWeight: 'bold',
    color: theme['color-basic-600'],
    padding: 5,
    textAlign: 'center',
    paddingBottom: 8
  },

  doctorButtonText: {
    fontSize: 22,
    color: '#3186FF',
  },

  card: {
    borderRadius: 15,
    width: '100%',
    height: '100%',
    padding: 15
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalCard: {
    borderRadius: 20,
    width: width - 50,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },


  textInput: {
    width: '100%',
    borderColor: 'transparent',
    paddingLeft: 0,
    borderBottomColor: theme['color-primary-500'], borderRadius: 0
  },
});

export default Dashboard;