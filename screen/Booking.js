import * as React from 'react';
import { StyleSheet,StatusBar,Dimensions } from 'react-native';
import { View } from 'react-native-animatable';
import { Avatar, Icon, Button, MenuItem, OverflowMenu, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components';
import BottomNavigator from './Components/BottomNavigator';

const MyStatusBar = ({ backgroundColor, ...props }) => (
    <View style={[{ backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);
const { height, width } = Dimensions.get('window');

export default class App extends React.Component {
 
    open(index) {
        console.log(index)
        this.props.navigation.navigate('Navigation');
        this.setState({selectedIndex:index})
    }
    render() {


        return (
            <View style={{justifyContent:'center', alignContent:'center',alignItems:'center',width: "100%", height: "100%" }}>
                <MyStatusBar backgroundColor={'transparent'} barStyle="dark-content" />
                
                <Text>BOOKING SCREEN</Text>
                
               
            </View>
        );
    }
}

const styles = StyleSheet.create({

});
