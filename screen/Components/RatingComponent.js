import React, { Component } from 'react';
import { Animated, StyleSheet, Easing } from 'react-native';
import { Image, View, Text } from 'react-native-animatable';
import { Icon } from '@ui-kitten/components';


class RatingComponent extends Component {
  

    render() {
        var rate = this.props.rate;
       
        return (

            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                        style={styles.icon}
                        fill={rate > 0 ? "#fab52a" : "#8a8a8a"}
                        name={rate > 0 ? "star" : "star-outline"}
                    />
                    <Icon
                        style={styles.icon}
                        fill={rate > 1 ? "#fab52a" : "#8a8a8a"}
                        name={rate > 1 ? "star" : "star-outline"}
                    />
                    <Icon
                        style={styles.icon}
                        fill={rate > 2 ? "#fab52a" : "#8a8a8a"}
                        name={rate > 2 ? "star" : "star-outline"}
                    />
                    <Icon
                        style={styles.icon}
                        fill={rate > 3 ? "#fab52a" : "#8a8a8a"}
                        name={rate > 3 ? "star" : "star-outline"}
                    />
                    <Icon
                        style={styles.icon}
                        fill={rate > 4 ? "#fab52a" : "#8a8a8a"}
                        name={rate > 4 ? "star" : "star-outline"}
                    />
                </View>
            </View>

        );
    }
}

export default RatingComponent;
const styles = StyleSheet.create({
    icon: {
        width: 18,
        height: 18,
        margin:2
    },
});