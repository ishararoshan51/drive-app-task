
import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { BottomNavigation, BottomNavigationTab, Icon } from '@ui-kitten/components';
import NavigationService from '../../Services/NavigationService';


class BottomNavigator extends Component {


    car = (props) => (
        <Icon style={styles.icon}  fill={"#4a4947"} name='car' />
    );

    navigation = (props) => (
        <Icon style={styles.icon}  fill={"#4a4947"} name='navigation-2' />
    );
    book = (props) => (
        <Icon style={styles.icon}  fill={"#4a4947"} name='book' />
    );
    setting = (props) => (
        <Icon style={styles.icon}  fill={"#4a4947"} name='settings-2' />
    );

    open(index) {
       
        switch (index) {
            case 0:
                NavigationService.navigate('Dashborad');
                break;
            case 1:
                NavigationService.navigate('Navigation');
                break;
            case 2:
                NavigationService.navigate('Booking');
                break;
            case 3:
                NavigationService.navigate('Setting');
                break;


        }
    }

    render() {
       
        return (

            <BottomNavigation
                style={styles.navigator}
                selectedIndex={-1}
                onSelect={inx => { this.open(inx) }}
            >
                <BottomNavigationTab icon={this.car} />
                <BottomNavigationTab icon={this.navigation} />
                <BottomNavigationTab icon={this.book} />
                <BottomNavigationTab icon={this.setting} />
            </BottomNavigation>

        );
    }
}
const styles = StyleSheet.create({
    navigator: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: '#7E88C4',
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.9,
        shadowRadius: 10.32,
        elevation: 20,
    },
    icon:{
        height:30,
        width:30,
    }
});
export default BottomNavigator;
