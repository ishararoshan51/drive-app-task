import React, { Component } from 'react';
import { Animated, StyleSheet, Easing } from 'react-native';
import { Image, View, Text } from 'react-native-animatable';


class LoadingIndicatorballs extends Component {
    constructor() {
        super();
    
    }

   
   
    render() {
      
        return (

            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View
                        animation={'bounce'}
                        duration={1500}
                        delay={0}
                        iterationCount="infinite"
                        style={[styles.boall, { backgroundColor: this.props.Color }]}></View>

                    <View
                        animation={'bounce'}
                        duration={1500}
                        delay={300}
                        iterationCount="infinite"
                        style={[styles.boall, { backgroundColor: this.props.Color }]}></View>

                    <View
                        animation={'bounce'}
                        duration={1500}
                        delay={600}
                        iterationCount="infinite"
                        style={[styles.boall, { backgroundColor: this.props.Color }]}></View>
                    <View
                        animation={'bounce'}
                        duration={1500}
                        delay={900}
                        iterationCount="infinite"
                        style={[styles.boall, { backgroundColor: this.props.Color }]}></View>
                </View>
            </View>

        );
    }
}

export default LoadingIndicatorballs;
const styles = StyleSheet.create({
    boall: {
        height: 4,
        width: 4,
        margin: 2,
        borderRadius: 50
    }
});