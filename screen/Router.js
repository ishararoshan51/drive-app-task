import {
    createAppContainer,
    createSwitchNavigator,
    NavigationActions
  } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';

import AuthLoadingScreen from './AuthLoading';
import Dashborad from './Dashboard';
import Navigation from './Navigation';
import Booking from './Booking';
import Setting from './Setting';

const Auth = createStackNavigator( // For Loading 
    {
        AuthLoadingScreen: AuthLoadingScreen,
    }
    , {
      headerMode: 'none'
    },
  
  );


  const MainScreens = createStackNavigator(
    {
      Dashborad:Dashborad,
      Navigation:Navigation,
      Booking:Booking,
      Setting:Setting
    }
    , {
      headerMode: 'none'
    },
  
  );

  export default createAppContainer(createSwitchNavigator(
    {
      Auth: Auth,
      MainScreens: MainScreens,
    },
    {
      initialRouteName: 'Auth',
    }
  ));