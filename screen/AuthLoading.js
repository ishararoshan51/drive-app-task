import * as React from 'react';
import {
  ActivityIndicator,
  StatusBar,
  Dimensions,
  StyleSheet,
  Alert,
  Platform,
  Linking,
  PermissionsAndroid,
  BackHandler
} from 'react-native';
import { Image, View, Text } from 'react-native-animatable';

import * as theme from './theme.json';
import LoadingIndicatorballs from './Components/LoadingIndicatorballs';


const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[{ backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class AuthLoadingScreen extends React.Component {


  async componentDidMount() {

    setTimeout(function () {
      this.props.navigation.navigate('Dashborad');
    }.bind(this), 2000);
   
  }

  render() {
    return (
      <View style={{ justifyContent: 'center', height: '100%', width: '100%', alignContent: 'center', alignItems: 'center' }}>
        <MyStatusBar backgroundColor={'transparent'} barStyle="dark-content" />
        <LoadingIndicatorballs Color={theme["color-primary-500"]}></LoadingIndicatorballs>
      </View>
    );
  }
}
const styles = StyleSheet.create({

}
)